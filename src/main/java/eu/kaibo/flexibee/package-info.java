/**
 * <p>Install missing dependencies.</p>
 * 
 * <pre>
 * mvn dependency:copy-dependencies -DoutputDirectory=/tmp
 * 
 * /etc/default/flexibee
 * CLASSPATH=/tmp/slf4j-nop-1.7.30.jar:/tmp/slf4j-api-1.7.30.jar
 * 
 * mvn javadoc:javadoc
 * </pre>
 */
package eu.kaibo.flexibee;